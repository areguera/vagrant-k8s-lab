# -*- mode: ruby -*-
# vi: set ft=ruby :

# Set the number of Kubernetes worker nodes virtual machines. These VMs run the
# kubelet and are exclusively reserved to host Kubernetes workloads.
WORKERNODES = 2

Vagrant.configure("2") do |config|
  config.ssh.insert_key = true
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # -------------------------------------------------------------------------------
  # Kubernetes control plane
  # -------------------------------------------------------------------------------
  config.vm.define "controlplane" do |node|
    node.vm.provider "virtualbox" do |v|
      # Please check requirements explained in the "Before you begin"
      # section at https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
      v.memory = 4096
      v.cpus = 2
    end

    node.vm.box = "areguera/k8s-lab-controlplane"
    node.vm.network "private_network", ip: "192.168.56.10"
    node.vm.hostname = "controlplane"

    node.vm.provision "ansible" do |ansible|
      ansible.playbook = "ansible/scenario_controlplane.yml"
      ansible.compatibility_mode = "1.8"
    end
  end

  # -------------------------------------------------------------------------------
  # Kubernetes worker nodes
  # -------------------------------------------------------------------------------
  (1..WORKERNODES).each do |i|
    config.vm.define "workernode-#{i}" do |node|

      node.vm.provider "virtualbox" do |v|
        # Please check requirements explained in the "Before you begin"
        # section at https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
        v.memory = 2048
        v.cpus = 2
      end

      node.vm.box = "areguera/k8s-lab-workernode"
      node.vm.network "private_network", ip: "192.168.56.#{i + 10}"
      node.vm.hostname = "workernode-#{i}"

      node.vm.provision "ansible" do |ansible|
        ansible.playbook = "ansible/scenario_workernode.yml"
        ansible.compatibility_mode = "1.8"
      end
    end
  end

end
