packer {
  required_plugins {
    vagrant = {
      version = "~> 1"
      source  = "github.com/hashicorp/vagrant"
    }
    ansible = {
      version = "~> 1"
      source = "github.com/hashicorp/ansible"
    }
  }
}

source "vagrant" "workernode" {
  global_id    = file(".vagrant/machines/workernode/virtualbox/index_uuid")
  box_name     = "workernode"
  provider     = "virtualbox"
  communicator = "ssh"
}

build {
  name    = "areguera-k8s-lab"
  sources = ["source.vagrant.workernode"]

  provisioner "ansible" {
    playbook_file   = "./workernode.yml"
    extra_arguments = [ "--scp-extra-args", "'-O'" ]
  }
}
