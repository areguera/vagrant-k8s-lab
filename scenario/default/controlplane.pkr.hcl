packer {
  required_plugins {
    vagrant = {
      version = "~> 1"
      source  = "github.com/hashicorp/vagrant"
    }
    ansible = {
      version = "~> 1"
      source = "github.com/hashicorp/ansible"
    }
  }
}

source "vagrant" "controlplane" {
  global_id    = file(".vagrant/machines/controlplane/virtualbox/index_uuid")
  box_name     = "controlplane"
  provider     = "virtualbox"
  communicator = "ssh"
}

build {
  name    = "areguera-k8s-lab"
  sources = ["source.vagrant.controlplane"]

  provisioner "ansible" {
    playbook_file   = "./controlplane.yml"
    extra_arguments = [ "--scp-extra-args", "'-O'" ]
  }
}
