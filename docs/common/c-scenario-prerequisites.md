## Pre-requisites

Before you can build and deploy Kubernetes clusters in this project, you need
to install the following software in your workstation:

- [GNU Make](https://www.gnu.org/software/make/)
- [Vagrant](https://www.vagrantup.com/)
- [Packer](https://www.packer.io/)
- [VirtualBox](https://www.virtualbox.org/)
- [Ansible](https://www.ansible.com/)
- [Kubectl](https://kubernetes.io/docs/reference/kubectl/)
