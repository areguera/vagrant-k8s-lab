### The scenario implementation

The scenario implements a learning environment with the following
characteristics:

- [Kubernetes](https://kubernetes.io/) v1.28
- [Containerd](https://github.com/containerd/containerd) in the Container Runtime Interface (CRI)
- [Flannel](https://github.com/flannel-io/flannel) with [host-gw backend](https://github.com/flannel-io/flannel/blob/master/Documentation/backends.md#host-gw) in the Container Network Interface (CNI)
- [MetalLB](https://metallb.org/) (LoadBalancer)

The scenario does not implement
[Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) or
[Gateway API](https://kubernetes.io/docs/concepts/services-networking/gateway/)
resources by default. If you want to interact with workload in your cluster,
you need to deploy one of these resources. See [configuring the cluster ingress
section](#configuring-the-cluster-ingress).
