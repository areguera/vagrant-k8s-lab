# Scenario Provisioning

This document describes conventions related to scenario provisioning inside the
`areguera-k8s-lab` project. If you want to install and configure software in the
virtual machines of a scenarios and don't know how to do it, this document
provides the information you need. If you want to know more about getting the
scenario virtual machines ready for provisioning, see [Scenario Deployment
(Vagrant + VirtualBox)](./README-Deployment.md) documentation.

## Overview

The scenario provisioning is written as code using [Ansible
Roles](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
in the `./k8s-roles` directory. In this directory there is one sub-directory
per role available. The roles for scenario provisioning are organized in two
categories: *"Kubernetes system roles"* and *"Kubernetes application roles"*.

## Kubernetes system roles

The Kubernetes system roles use the suffix `k8s-sys-` in the name. They are
written to control the Kubernetes cluster desired state including changes at
operating system level (e.g., manage packages, services, and system
configurations.) This type of role doesn't change the state of applications
running on the Kubernetes cluster.

* [`k8s-sys-common`](./k8s-roles/k8s-sys-common/) --- Kubernetes cluster common configurations tasks for all nodes.
* [`k8s-sys-controlplane`](./k8s-roles/k8s-sys-controlplane/) --- Kubernetes cluster specific tasks to control plane only.
* [`k8s-sys-workernode`](./k8s-roles/k8s-sys-workernode/) --- Kubernetes cluster specific tasks to worker nodes only.
* [`k8s-sys-workstation`](./k8s-roles/k8s-sys-workstation/) --- Student workstation specific tasks.

## Kubernetes application roles

The Kubernetes application roles use the suffix `k8s-app-` in the name. They
provision new Kubernetes applications in the Kubernetes cluster. The roles in
this category don't make changes in the cluster configuration itself. However,
roles in this category can change the underlying operating system to satisfy
the application needs (e.g., installing system services and change related
configuration files).

* [`k8s-app-jekyll`](./k8s-roles/k8s-app-jekyll/) --- Publish Jekyll projects on Kubernetes cluster.
