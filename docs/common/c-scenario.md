## Scenarios

This project uses the word "scenario" to organize unique implementations of
Kubernetes clusters. The scenario provides a unique context for learning
Kubernetes technology. It is conceived under the [KISS
principle](https://en.wikipedia.org/wiki/KISS_principle). As you progress in
your learning journey, you can customize existent scenario configurations
yourself to build new learning contexts and strengthen your skills by
practicing.

Documentation related to each scenario is available in the table below:

| Name    | Description                                              |
| ------- | -------------------------------------------------------- |
| [default](docs/scenarios/default/c-scenario.md) | Kubernetes cluster where other scenarios are built upon. |
