# Scenario Deployment

This document describes conventions related to scenario deployments inside
`areguera-k8s-lab` project. If you want to deploy a brand new scenarios and
don't know how to do it, this document provides the information you need. If
you want to know more about installing and configuring software in the scenario
virtual machines, see [Scenario Provisioning (Vagrant +
Ansible)](./README-Provisioning.md) documentation.

## Overview

The scenario deployment is written as code using Vagrant and implemented on
VirtualBox. Writing a new scenario deployment demands planning about the number
of virtual machines required to implement the Kubernetes cluster you are
willing to have, as well as any auxiliary virtual machine you may need to
implement specific concepts other than the cluster itself (e.g., workstation
for management, monitoring system, etc.). Some of the questions you need to
answer are: How many control planes and worker nodes the cluster will have? How
many `etcd` servers will exist? Are the cluster components installed as service
on the system or as Pods in the cluster itself? Does the cluster case study
need auxiliary hosts? The answer to these questions will help you to define the
number of virtual machines you need to create and ther resources. Creating your
own cluster definition is a good exercise to understand its components and how
they interact one another.

# `./k8s-clusters/${NAME}/`

The `./k8s-clusters/${NAME}/` stores the source code used for scenario deployments, where
`${NAME}` is a [DNS subdomain name](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-subdomain-names).
In this directory, you store the scenario `Vagrantfile` and all its
auxiliary files.

The scenario directory structure convention is the following:

```
k8s-clusters/${NAME}/
├── README.md
├── Vagrantfile
├── ansible.cfg
├── docs
│   ├── Makefile
│   ├── scenario.png
│   └── scenario.svg
├── provisioning
│   ├── files
│   │   ├── kubeadm-join-command.sh
│   │   └── kubectl-config.yml
│   ├── k8s-sys-controlplane.yml
│   ├── k8s-sys-workernode.yml
│   ├── k8s-sys-workstation.yml
│   └── vagrant-fixes.yml
└── requirements.yml
```

## `./Vagrantfile`

The `./Vagrantfile` is where you define the number of virtual machines your
cluster will have and the provisioning playbooks associated to them.

To know more, see the [Vagrantfile documentation](https://developer.hashicorp.com/vagrant/docs/vagrantfile).

## `./ansible.cfg`

The `./ansible.cfg` allows you to customize how Ansible behaves.  In the
context of `areguera-k8s-lab` project, it is used to customize the path of roles
and disable the SSH key verification to prevent. This allows different
scenarios to reuse the same provisioning roles and access the virtual machines
without any question that may interrupt the provisioning flow.

Example:

```
[defaults]
host_key_checking = False
roles_path = ../../k8s-roles
```

To know more, see the [Ansible configuration settings documentation](https://docs.ansible.com/ansible/latest/reference_appendices/config.html).

## `./requirements.yml`

The `.requirements.yml` file defines the Ansible dependencies your scenario
needs for a successful provisioning.

To know more about the Ansible `requirements.yml`, see:
* [Installing roles and collections from the same requirements.yml file](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#installing-roles-and-collections-from-the-same-requirements-yml-file)
* [Install multiple collections with a requirements file](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#install-multiple-collections-with-a-requirements-file)

## `./provisioning/`

The `./provisioning/` directory provides provisioning playbooks for all different virtual
machine types you have in your scenario. Examples of provisioning playbooks include the following:
* `k8s-sys-controlplane.yml` --- playbook used to provision control plane virtual machines.
* `k8s-sys-workernode.yml` --- playbook used to provision worker nodes virtual machines.
* `k8s-sys-student.yml` --- playbook used to provision student workstation virtual machines.

For example, consider the following provisioning playbook:

```yaml
# k8s-clusters/default/provisioning/k8s-sys-controlplane.yml
---
- hosts: all
  become: true

  roles:
    # --------------------------------------------------------------------------
    # Kubernetes cluster - System (required states)
    # --------------------------------------------------------------------------
    - k8s-sys-common
    - k8s-sys-controlplane
    # --------------------------------------------------------------------------
    # Kubernetes cluster - Applications (optional states)
    # --------------------------------------------------------------------------
    #- role: "k8s-app-jekyll"
    #  project_name: "areguera"
    #  project_namespace: "jekyll"
    #  project_git_url: "https://gitlab.com/areguera/areguera.net.git"
```

In this configuration, both `k8s-sys-common` and `k8s-sys-controlplane` roles
are applied during cluster deployment while `k8s-app-jekyll` role is not. To
apply this role, uncomment its block before running the `vagrant up` command,
or after `vagrant up` command, running the `vagrant provision controlplane`
command. Commenting a role that was previously applied during deployment does
not revert its changes. In case you want to start fresh again, you need to
destroy and create the cluster with the right set of roles you want to apply.

## `./provisioning/files/`

The `./provisioning/files/` directory provides auxiliary files produced during
scenario provisioning. Examples of files produced during scenario provisioning
include the following:
* `kubeadm-config.yml` --- The configuration file you need to manage the cluster using `kubectl` command.
* `kubeadm-join-command.sh` --- Shell script with the command that allows you to join worker nodes into the cluster.

## `./README.md`

The `./README.md` file provides the scenario documentation entry point. The
content written in this file is concise and can be expanded adding links to
other `.md` files under `./docs/`.

## `./docs/`

The `./docs/` directory provides auxiliary documentation files (e.g., `.svg`, `.png`, and `.md`).
