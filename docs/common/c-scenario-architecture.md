### The scenario architecture

The scenario runs on three virtual machines, one for controlplane and two for
worker nodes. The controlplane node is tainted to prevent resources from being
scheduled inside it. The worker nodes are untainted to receive all the cluster
workload. The virtual machines run CentOS Stream 9 operating system. These
virtual machines consume CPU, RAM and storage from your workstation.

The following picture illustrates the default scenario architecture:

![scenario](docs/img/scenario.png)

The following table describes the resources the scenario consumes in your
workstation:

| VMs          | CPUs | RAM | Storage (Dynamic Allocation) |
| ------------ | ---- | --- | ---------------------------- |
| controlplane | 2    | 4GB | 4.1GB (up to 128GB)          |
| workernode-1 | 2    | 2GB | 3.1GB (up to 128GB)          |
| workernode-2 | 2    | 2GB | 3.1GB (up to 128GB)          |
