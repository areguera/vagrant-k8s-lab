---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: areguera-k8s-lab
title_lead: Open environment for learning Kubernetes and surrounding technologies.

with_highlight: stackoverflow-light
with_breadcrumbs: false
---
