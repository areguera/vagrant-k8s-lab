# The `default` scenario

The `default` scenario uses  the
[`kubeadm`](https://kubernetes.io/docs/reference/setup-tools/kubeadm/) command
to deploy a Kubernetes cluster with three nodes, one control plane and two
worker nodes. The cluster uses [containerd](https://containerd.io) in the CRI,
and [Flannel](https://github.com/flannel-io/flannel) configured with
[host_gw](https://github.com/flannel-io/flannel/blob/master/Documentation/backends.md#host-gw)
backend in CNI plugin. Additionally, the default scenario configures metallb to
implement the cluster LoadBalancer, and Isitio to implement the ingress
controller. The operating system of all virtual machines is [CentOS
Stream](https://www.centos.org/centos-stream/) 9.

![The default scenario](docs/scenario.png)
