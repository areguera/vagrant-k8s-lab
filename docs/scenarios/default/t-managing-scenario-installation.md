### Managing scenario installation

1. Take a scenario snapshot to you create a point in time where you can come
   back later (~0.40 minutes). This is very useful when you want to make risky
   tests and don't want to loose all the work you've done so far.

   ```
   make scenario-deploy-snapshot-push
   ```

1. Restore the last scenario snapshot taken (~1.29 minutes).

   ```
   make scenario-deploy-snapshot-pop
   ```

1. Suspend the scenario to continue later where you left it (~0.16 minutes):

   ```
   make scenario-deploy-suspend
   ```

1. Resume the scenario from a suspended state (~0.25 minutes):

   ```
   make scenario-deploy-resume
   ```

1. Halt the scenario to continue later where you left it (~1.36 minutes):

   ```
   make scenario-deploy-halt
   ```

1. Destroy the scenario and all the work done inside it (~0.20 minutes):

   ```
   make scenario-deploy-destroy
   ```

## Additional resources

- The scenario images
- The scenario deployment

### Configuring the cluster ingress

To configure the cluster ingress, do the following:

1. Edit the `./scenario/deploy/host_vars/controlplane.yml` file and be sure the
`controlplane_kubernetes_ingress` variable is defined there. Possible values
are:

- `ingress-nginx` --- Uses Kubernetes Ingress resource to manage ingress traffic.
- `istio` -- (preferred) Uses Istio service mesh to manage ingress traffic.

1. Re-deploy the controlplane to apply the changes:

   ```
   make scenario-deploy-controlplane
   ```

1. Configure the `/etc/hosts` file in your workstation. This enables the host
   you use to send ingress traffic into the cluster. For example, if you want
   to send traffic into the cluster using `http://myapp.areguera-k8s-lab` host,
   the `/etc/hosts` configuration would look like the following: values:

   ```
   192.168.56.5 myapp.areguera-k8s-lab
   192.168.56.6 myapp.areguera-k8s-lab
   192.168.56.7 myapp.areguera-k8s-lab
   ```

That is all. At this point you should be ready to start practicing in a clean,
fresh and fully operative Kubernetes cluster.

### Building the scenario images

To build the scenario images, do the following:

1. Run make `scenario-build` Build the vagrant box images you will use to built the cluster nodes'
   virtual machines (~16.18 minutes):

   ```
   make scenario-build
   ```

   You execute this command only once, unless you want to introduce changes
   into the images. In such case, you need to run the command again to rebuild
   the images you use in `scenario-deploy`.

1. Deploy the virtual machines where the cluster nodes will run (~3.28 minutes):

   ```
   make scenario-deploy
   ```
