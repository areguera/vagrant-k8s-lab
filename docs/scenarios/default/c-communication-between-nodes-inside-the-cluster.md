## Communication between nodes inside the cluster

The communication between nodes happens through `eth1` network interface. The
`eth1` interface doesn't exist in VirtualBox default network layout. The
`Vagrantfile` has been written to add it automatically during the cluster
initialization, when the virtual machines are created.

![Cluster Nodes Network](docs/cluster-nodes-network-2.png)

The `eth1` configuration in `Vagrantfile` uses the
[private_network](https://developer.hashicorp.com/vagrant/docs/networking/private_network)
identifier to use the `192.168.56.0/24` network address, and assign the host
addresses predictably. The convention for assigning host addresses in
`192.168.56.0/24` network is as follows: the control plane node always has the
`192.168.56.10/24` IP address, while worker nodes get their IP addresses based
on its number, starting from `192.168.56.11/24` for `node-1`,
`192.168.56.12/24` for `node-2`, and so on.
