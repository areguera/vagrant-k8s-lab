## Communication between nodes and the external world

The communication between nodes and the external world happens through `eth0`
network interface. VirtualBox automatically attaches the `eth0` interface on
new virtual machines and configures it in NAT mode, using the `10.0.2.15/24` IP
address, and the `10.0.2.2/24` default gateway. This is known as [VirtualBox default
network layout](https://www.virtualbox.org/manual/ch03.html#settings-network).

![Cluster Nodes Network](docs/cluster-nodes-network-1.png)

VirtualBox default network layout may look a bit confusing at first sight, when
you see different virtual machines having the same `10.0.2.15/24` IP, and MAC
address attached to the same `10.0.2.0/24` network address. This seems to be a
VirtulBox design choice to allow virtual machines to communicate to the
external world using a consistent IP addressing schema. In this design,
connections between virtual machines are treated individually. Each virtual
machine is connected to its own `10.0.2.0/24` network inside VirtualBox,
probably using a unique link for each one internally.
