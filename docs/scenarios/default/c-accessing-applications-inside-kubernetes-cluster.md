## Accessing applications inside Kubernetes cluster

In Kubernetes, you need to create the following resources and their
dependencies to make your application public to clients outside the cluster:

* `Deployment` resource, to run you application resiliently. The related
  container image of your application is must exist first in a registry for
  you to deploy it here.

* `Service` resource, to expose your application deployment inside the cluster.

* `Ingress` resource, to expose your service outside the cluster using the
  [`LoadBalancer`](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)
  type.

Neither `Deployment` nor `Service` has external dependencies to do their work.
The `Ingress` resource, however, does have. The ingress resource needs an
*Ingress Controller* that Kubernetes doesn't ship by default. In order for the
ingress resource to work, you must pick one ingress controller from a [list of
many](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)
available and install it in your cluster first. Otherwise, without an ingress
controller installed in the cluster, the ingress resource does nothing. The
`default.{{ cluster_name }}` scenario installs the [nginx ingress
controller](https://kubernetes.github.io/ingress-nginx/) automatically for you,
during cluster provisioning.

Another consideration is about how the cluster balances the load. Kubernetes
doesn't ship a load balancer that can be used in a bare-metal implementation
like the one we are building here with Vagrant and VirtualBox. Such
functionality is only available when you run Kubernetes on cloud providers like
AWS, GCP and Azure. Fortunately enough, the [metalLB](https://metallb.org/)
network load balancer exists to satisfy this specific use case. The
`default.vagrant-k8-lab` scenario installs it to enable the `LoadBalance`
ingress type in the cluster.

In case you don't install a cluster load balancer, the `LoadBalance` type won't
work and the number of options left to access applications deployed inside the
Kubernetes cluster are reduced to `Service` resources configured with
`NodePort` type and `externalIP`. This configuration doesn't reflect real
production scenarios where you access your application through one domain name
and the requests are received by one or more hosts that redirect traffic to
Kubernetes ingress objects. See nginx ingress controller [bare-metal
considerations](https://kubernetes.github.io/ingress-nginx/deploy/baremetal/).

The `default.{{ cluster_name }}` scenario is also a domain name. It is configured
in the controle plane node using dnsmasq to associate the IP range
(`192.168.56.5-192.168.56.7`) exposed by MetalLB. This domain name only works
inside the `192.168.56.0/24` network and its purpose is offering the student a
close simulation of real production environments when you try to access an
application deployed inside a Kubernetes cluster.

For example, consider what happens when you make an HTTP request to access an
application configured behind `default.{{ cluster_name }}` domain name. The
client's operating system uses the name server configuration to ask for the IP
address associated. Later, when the domain name IP address is known, it asks
again what's the ARP address of that IP address. MetalLB responds with the
associated ARP address the client's operating system needs to initiate the HTTP
requests against the service running the application inside the Kubernetes
cluster.

![Communication with the cluster services](docs/cluster-ingress.png)

The traffic details may help to better appreciate what's happening on this
simplified exchange between the student host and the control plane host:

```
[vagrant@student ~]$ sudo tshark -n -i eth1
Running as user "root" and group "root". This could be dangerous.
Capturing on 'eth1'
    1 0.000000000 192.168.56.9 → 192.168.56.10 DNS 87 Standard query 0x6313 A default.{{ cluster_name }}
    2 0.002271645 192.168.56.10 → 192.168.56.9 DNS 135 Standard query response 0x6313 A default.{{ cluster_name }} A 192.168.56.5 A 192.168.56.6 A 192.168.56.7
    3 0.002540049 192.168.56.9 → 192.168.56.10 DNS 87 Standard query 0x4b14 AAAA default.{{ cluster_name }}
    4 0.004274843 192.168.56.10 → 192.168.56.9 DNS 87 Standard query response 0x4b14 AAAA default.{{ cluster_name }}
    5 0.006948794 08:00:27:cb:49:de → ff:ff:ff:ff:ff:ff ARP 42 Who has 192.168.56.5? Tell 192.168.56.9
    6 0.008103626 08:00:27:c1:1e:e5 → 08:00:27:cb:49:de ARP 60 192.168.56.5 is at 08:00:27:c1:1e:e5
    7 0.008117773 192.168.56.9 → 192.168.56.5 TCP 74 58202 → 80 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 SACK_PERM=1 TSval=1003652782 TSecr=0 WS=128
    8 0.008760232 192.168.56.5 → 192.168.56.9 TCP 74 80 → 58202 [SYN, ACK] Seq=0 Ack=1 Win=65160 Len=0 MSS=1460 SACK_PERM=1 TSval=1336703289 TSecr=1003652782 WS=128
    9 0.008836135 192.168.56.9 → 192.168.56.5 TCP 66 58202 → 80 [ACK] Seq=1 Ack=1 Win=64256 Len=0 TSval=1003652783 TSecr=1336703289
   10 0.010139867 192.168.56.9 → 192.168.56.5 HTTP 157 GET / HTTP/1.1
   11 0.010863830 192.168.56.5 → 192.168.56.9 TCP 66 80 → 58202 [ACK] Seq=1 Ack=92 Win=65152 Len=0 TSval=1336703291 TSecr=1003652785
   12 0.016657910 192.168.56.5 → 192.168.56.9 HTTP 330 HTTP/1.1 200 OK  (text/html)
   13 0.016777806 192.168.56.9 → 192.168.56.5 TCP 66 58202 → 80 [ACK] Seq=92 Ack=265 Win=64128 Len=0 TSval=1003652791 TSecr=1336703297
   14 0.022317588 192.168.56.9 → 192.168.56.5 TCP 66 58202 → 80 [FIN, ACK] Seq=92 Ack=265 Win=64128 Len=0 TSval=1003652797 TSecr=1336703297
   15 0.023363576 192.168.56.5 → 192.168.56.9 TCP 66 80 → 58202 [FIN, ACK] Seq=265 Ack=93 Win=65152 Len=0 TSval=1336703304 TSecr=1003652797
   16 0.023393332 192.168.56.9 → 192.168.56.5 TCP 66 58202 → 80 [ACK] Seq=93 Ack=266 Win=64128 Len=0 TSval=1003652798 TSecr=1336703304
```

### Domain names and ingress rules

In Kubernetes, the `Ingress` resource operates based on *rules*. Each rule is
made of `Host`, `Path`, and `Backends`. The `Ingress` resource compares the
value of `Host` and the `Path` against the respective values received in the
HTTP request directed to the `Ingress` resource. Based on the match it finds,
the `Ingress` resource sends that particular request to one service `Backend`
or another. There are other configuration properties, including [Path
Types](https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types)
and [Hostname
Wildcards](https://kubernetes.io/docs/concepts/services-networking/ingress/#hostname-wildcards),
that may affect the way a rule matches. However, the `default.{{ cluster_name }}`
scenario doesn't consider them to use default values.  You send the HTTP
request traffic to that `Backend` exactly matching the respective values of
`Host` and `Path` you configured.

When you create your ingress resources in the cluster, the
`default.{{ cluster_name }}` scenario configures the `default.{{ cluster_name }}`
domain name for you to have one ready to start with. It is the entry point for
the applications you deploy, expose and configure the ingress resource to
serve. In the `Host` part of the Ingress rule, you should set the
`default.{{ cluster_name }}` domain name as value because it is the only one
available by default.

When you use the `default.{{ cluster_name }}` domain name, it only allows you to
test different `paths` in your ingress rules. If you want to test different
domain names in the ingress rules configuration, you need to add the domain
names you want to use into the DNS server configured in the control plane. This
way, it can respond the IP address the load balancer assigned to the ingress
resource once created. Otherwise, it may not return the correct IP address, and
the communication with it fails.

The `default.{{ cluster_name }}` domain name and others you create to fit your
needs must be associated to the IP addresses exposed by the cluster network
load balancer, always. These IP addresses are `192.168.56.5`, `192.168.56.6`,
`192.168.56.7`.

### Customizing domain names

The `default.{{ cluster_name }}` scenario installs and configures `dnsmasq` in the
control plane node, outside the Kubernetes cluster, to resolve custom domain
names under `.{{ cluster_name }}`. To make this service available, the `default`
scenario configures the virtual machines during provisioning to query the
control plane name server (`192.168.56.10`) first, and the Virtualbox internal
name server (`10.0.2.3`) later, in that specific order. This way, all virtual
machines will firstly query the name server where you can introduce
customization and then, if nothing is found here, the name server responsible
of forwarding queries to Internet.

To customize the control plane name server, do the following:

1. SSH into control plane
   ```
   vagrant ssh controlplane
   ```
2. Customize the domain names you want to have here (e.g., adding
   `app1.default.{{ cluster_name }}`) in the `/etc/hosts` file. Remember to always
   assign the three IP addresses used by the cluster network load balancer. It
   can assign any of them to your ingress resource so, all of them must be
   associated to your domain name.
   ```
   192.168.56.5 app1.default.{{ cluster_name }}
   192.168.56.6 app1.default.{{ cluster_name }}
   192.168.56.7 app1.default.{{ cluster_name }}
   ```
4. Restart `dnsmasq` service to apply changes.
   ```
   sudo systemctl restart dnsmasq.service
   ```

### Creating your first application in Kubernetes cluster

The ingress, service, and application deployment objects related to
`default.{{ cluster_name }}` don't exist when you initialize the `default`
scenario. You need to create them first.

To create the `default.{{ cluster_name }}` application, you need to:

1. SSH into the `student` node (`192.168.56.9`).
   ```
   vagrant ssh student
   ```

2. Create a deployment with your application. This example uses httpd image and
   4 replicas, but you can change it to whatever your application be and need.
   ```
   kubectl create deployment/httpd --image=httpd --replicas=4
   ```

3. Expose your application deployment. This action creates a service object in
   Kubernetes cluster using the deployment name, and creates the association
   between them.
   ```
   kubectl expose deployment/httpd --port=80
   ```

4. Create the ingress object to expose the service outside the cluster.
   ```
   kubectl create ingress default.{{ cluster_name }} \
    --rule=default.{{ cluster_name }}/=httpd:80 \
    --class=nginx
   ```

5. Test your application.
   ```
   curl http://default.{{ cluster_name }}/
   ```
   and the output for httpd example should be:
   ```
   <html><body><h1>It works!</h1></body></html>
   ```
   Congratulations! 🎉 You've deployed your first application in a Kubernetes cluster.

### Using ingress rules (to decouple applications)

At this point you should have an idea about the relation between ingress,
services and deployment objects in Kubernetes. In the previous example, the
ingress object redirects all traffic to a single service object. That's simple
and works well for some small application cases. However, when your application
starts growing, it is not a good practice to have it all in a single monumental
container but in smaller independent ones. This is the holy grail of
microservice architectures, *decouple* application components and allow
communication between them. It resembles the [Unix toolbox
principle](https://en.wikipedia.org/wiki/Unix_philosophy), where you can pipe
different commands to achieve results that none of the commands individually
can achieve on its own.

In Kubernetes, the ingress object allows us to decouple our application using
*rules*. For example, in the previous example when you access
`http://default.{{ cluster_name }}/`, note that there is a single rule, the
one you passed at creation time. So, there is a one unique way of accessing
your application.

```
Name:             default.{{ cluster_name }}
Labels:           <none>
Namespace:        default
Address:          192.168.56.5
Ingress Class:    nginx
Default backend:  <default>
Rules:
  Host                         Path  Backends
  ----                         ----  --------
  default.{{ cluster_name }}
                               /   httpd:80 (10.244.1.2:80,10.244.1.3:80,10.244.2.2:80 + 1 more...)
Annotations:                   <none>
Events:
  Type    Reason  Age                 From                      Message
  ----    ------  ----                ----                      -------
  Normal  Sync    22s (x7 over 112m)  nginx-ingress-controller  Scheduled for sync
```

> 🎓 **Assignment:** Create one `nginx` deployment, expose it on port `80`, and
> update the ingress object `default.{{ cluster_name }}` to attend it alongside
> the already existent `httpd` deployment. There are two ways of doing this,
> using host names or paths, each one has its own particularities.

## Known issues

* [Kubernetes network policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
  doesn't work. That's expected since Flannel plugin is being used in the CNI.

* The nginx ingress controllers doesn't rewrite paths unless you configure the
  following `annotations` in the ingress resource:
  ```
  nginx.ingress.kubernetes.io/rewrite-target: /
  ```

### Additional resources

* https://kubernetes.github.io/ingress-nginx/deploy/
* https://metallb.org/
* https://kubernetes.io/docs/concepts/services-networking/ingress/
* https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/
