## Communication between pods inside different cluster nodes

![Cluster Nodes Network](docs/cluster-nodes-network-3.png)

When pods are in the same node, the communication happens locally, through the
`cni0` network virtual interface in the node. In these cases, the network
traffic never leaves the node. For example, consider you have two containers,
`10.244.1.2` and `10.244.1.3`, running both in `node-1`. You want to send
traffic from `10.244.1.2` to `10.244.1.3`. When you execute a `traceroute`
command inside `10.244.1.2` container, you will see see only 1 hop:

```
/ # traceroute -n 10.244.1.3
traceroute to 10.244.1.3 (10.244.1.3), 30 hops max, 46 byte packets
 1  10.244.1.3  0.009 ms  0.005 ms  0.005 ms
```

When pods are in different nodes, the communication happens by routing traffic
between virtual networks, through the `eth1` on each node. The routing table
required for this traffic is created automatically, when the node is attached
to the cluster. For example, consider the container `10.244.2.2` is running on
`node-2` and the container `10.244.1.2` is running on `node-1`. When you
execute a `traceroute` command inside `10.244.2.2` container, you will see 3
hops exactly:

```
traceroute to 10.244.1.2 (10.244.1.2), 30 hops max, 46 byte packets
 1  10.244.2.1  0.009 ms  0.008 ms  0.006 ms
 2  192.168.56.11  1.010 ms  0.758 ms  0.972 ms
 3  10.244.1.2  1.246 ms  0.969 ms  1.220 ms
```

A closer look at the route table of each node reveals the path taken by
packages when they are sent locally in the same node or to a different node.

```
---------------------------------------------------------------------------------
controlplane
---------------------------------------------------------------------------------
default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
10.244.0.0/24 dev cni0 proto kernel scope link src 10.244.0.1
10.244.1.0/24 via 192.168.56.11 dev eth1
10.244.2.0/24 via 192.168.56.12 dev eth1
192.168.56.0/24 dev eth1 proto kernel scope link src 192.168.56.10 metric 101
---------------------------------------------------------------------------------
node-1
---------------------------------------------------------------------------------
default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
10.244.0.0/24 via 192.168.56.10 dev eth1
10.244.1.0/24 dev cni0 proto kernel scope link src 10.244.1.1
10.244.2.0/24 via 192.168.56.12 dev eth1
192.168.56.0/24 dev eth1 proto kernel scope link src 192.168.56.11 metric 101
---------------------------------------------------------------------------------
node-2
---------------------------------------------------------------------------------
default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
10.244.0.0/24 via 192.168.56.10 dev eth1
10.244.1.0/24 via 192.168.56.11 dev eth1
10.244.2.0/24 dev cni0 proto kernel scope link src 10.244.2.1
192.168.56.0/24 dev eth1 proto kernel scope link src 192.168.56.12 metric 101
```

### Additional resources

* https://www.tkng.io/
* https://www.tkng.io/cni/flannel/
* https://github.com/flannel-io/flannel
* https://osi-model.com/data-link-layer/
