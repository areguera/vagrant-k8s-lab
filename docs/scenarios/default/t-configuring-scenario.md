### Configuring scenario

To configure the Kubernetes cluster scenario in your workstation, set the
`KUBECONFIG` environment variable to point to `files/kubectl.config` file.

```
export KUBECONFIG=${PWD}/files/kubectl.config
```
