### Validating the scenario

To validate the scenario installation, do the following:

```
kubectl get all -A
```

This command should output something similar to the following:

```
NAMESPACE      NAME                                       READY   STATUS    RESTARTS        AGE
kube-flannel   pod/kube-flannel-ds-bxgck                  1/1     Running   1 (5m15s ago)   56m
kube-flannel   pod/kube-flannel-ds-mz9bd                  1/1     Running   2 (6m39s ago)   73m
kube-flannel   pod/kube-flannel-ds-vsj94                  1/1     Running   2 (3m35s ago)   56m
kube-system    pod/coredns-5dd5756b68-dvkkn               1/1     Running   1 (5m15s ago)   73m
kube-system    pod/coredns-5dd5756b68-m9rmg               1/1     Running   1 (5m15s ago)   73m
kube-system    pod/etcd-controlplane                      1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-apiserver-controlplane            1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-controller-manager-controlplane   1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-proxy-k68wp                       1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-proxy-ldjcn                       1/1     Running   1 (5m15s ago)   56m
kube-system    pod/kube-proxy-m4qq7                       1/1     Running   1 (6m ago)      56m
kube-system    pod/kube-scheduler-controlplane            1/1     Running   2 (6m39s ago)   73m

NAMESPACE     NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  73m
kube-system   service/kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   73m

NAMESPACE      NAME                             DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-flannel   daemonset.apps/kube-flannel-ds   3         3         3       3            3           <none>                   73m
kube-system    daemonset.apps/kube-proxy        3         3         3       3            3           kubernetes.io/os=linux   73m

NAMESPACE     NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   deployment.apps/coredns   2/2     2            2           73m

NAMESPACE     NAME                                 DESIRED   CURRENT   READY   AGE
kube-system   replicaset.apps/coredns-5dd5756b68   2         2         2       73m
```
