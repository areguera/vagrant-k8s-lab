### Installing the scenario

To install the scenario in your workstation, run the `make` command passing the
scenario name to `SCENARIO` variable in the repository root directory.

```
make SCENARIO=default
```
