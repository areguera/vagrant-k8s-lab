# areguera-k8s-lab

This project allows you to build and deploy Kubernetes clusters in your
workstation using infrastructure-as-code (Vagrant + VirtualBox + CentOS
Stream), configuration-as-code (Ansible), and the term "Scenario" to
conceptually organize specific cluster's configuration designs. This project is
for anyone willing to take full control over the learning environment.

## Pre-requisites

On the hardware side, your workstation needs to have enough resources (i.e.,
CPU, RAM and Disk space) to handle the load of building, deploying and running
a Kubernetes cluster. Minimum resources are 8 CPUs, 8GB RAM, 10GB disk space.
You may need to tune these values based on the number of resources you intend
to deploy in the cluster.

On the software side, you need to install the following software:

- [GNU Make](https://www.gnu.org/software/make/)
- [Vagrant](https://www.vagrantup.com/)
- [Packer](https://www.packer.io/)
- [VirtualBox](https://www.virtualbox.org/) (check [supported platform packages](https://www.virtualbox.org/wiki/Downloads))
- [Ansible](https://www.ansible.com/)
- [Kubectl](https://kubernetes.io/docs/reference/kubectl/)

## Usage

### Installation

![scenario](docs/assets/img/scenario.png)

To build and deploy the `default` scenario in your workstation, do the
following:

1. Run the `make` command with `SCENARIO=default` argument.

   ```
   make SCENARIO=default
   ```

1. Add the scenario's configuration file to kubectl configuration:

   ```
   export KUBECONFIG=${KUBECONFIG}:${PWD}/files/default.areguera-k8s-lab.config
   ```

### Validation

To validate the `default` scenario is up an running, do the following:

```
kubectl get all -A
```

This command should output something similar to the following:

```
NAMESPACE      NAME                                       READY   STATUS    RESTARTS        AGE
kube-flannel   pod/kube-flannel-ds-bxgck                  1/1     Running   1 (5m15s ago)   56m
kube-flannel   pod/kube-flannel-ds-mz9bd                  1/1     Running   2 (6m39s ago)   73m
kube-flannel   pod/kube-flannel-ds-vsj94                  1/1     Running   2 (3m35s ago)   56m
kube-system    pod/coredns-5dd5756b68-dvkkn               1/1     Running   1 (5m15s ago)   73m
kube-system    pod/coredns-5dd5756b68-m9rmg               1/1     Running   1 (5m15s ago)   73m
kube-system    pod/etcd-controlplane                      1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-apiserver-controlplane            1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-controller-manager-controlplane   1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-proxy-k68wp                       1/1     Running   2 (6m39s ago)   73m
kube-system    pod/kube-proxy-ldjcn                       1/1     Running   1 (5m15s ago)   56m
kube-system    pod/kube-proxy-m4qq7                       1/1     Running   1 (6m ago)      56m
kube-system    pod/kube-scheduler-controlplane            1/1     Running   2 (6m39s ago)   73m

NAMESPACE     NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
default       service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP                  73m
kube-system   service/kube-dns     ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   73m

NAMESPACE      NAME                             DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kube-flannel   daemonset.apps/kube-flannel-ds   3         3         3       3            3           <none>                   73m
kube-system    daemonset.apps/kube-proxy        3         3         3       3            3           kubernetes.io/os=linux   73m

NAMESPACE     NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   deployment.apps/coredns   2/2     2            2           73m

NAMESPACE     NAME                                 DESIRED   CURRENT   READY   AGE
kube-system   replicaset.apps/coredns-5dd5756b68   2         2         2       73m
```

The `default` scenario does not support ingress traffic into the cluster. This
is intential to keep it as simple as possible. To enable ingress traffic into
the cluster, you can enable it yourself changing the configuration files used
to build the images or the files controlling image deployment. When you enable
one of the ingress options, you need to increase both CPU and RAM values
assigned to controlplane in Vagrantfile accordingly, so it can handle the load.

## Documentation

To know more about how to build, deploy and configure scenarios, check the [project's documentation](https://areguera.gitlab.io/areguera-k8s-lab/).

## Contribute

This project is open to receive your contributions. Feel free to [open an
issue](https://gitlab.com/areguera/areguera-k8s-lab/-/issues) describing what
you would like to change. Or even better, fork the repository and create merge
requests to propose your exact changes.

## License

MIT License

Copyright (c) 2024 Alain Reguera Delgado
