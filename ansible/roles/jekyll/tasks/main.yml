---
# tasks file for kubernetes-deploy-jekyll

# ------------------------------------------------------------------------------
# System configuration
# ------------------------------------------------------------------------------
- name: "{{ project_name }} | The NFS server is started and enabled at boot time"
  ansible.builtin.service:
    name: nfs-server.service
    state: started
    enabled: true

- name: "{{ project_name }} | The NFS project-shared directory structure is present"
  ansible.builtin.file:
    path: "/srv/exports/jekyll-bundles"
    state: directory
    owner: vagrant
    group: vagrant
    mode: 0755
  notify:
    - restart-dnsmasq-server

- name: "{{ project_name }} | The NFS project-specific directory structure is present"
  ansible.builtin.file:
    path: "/srv/exports/{{ item.dirname }}/{{ project_name }}"
    state: directory
    owner: vagrant
    group: vagrant
    mode: 0755
  loop:
    - {dirname: "jekyll-repos"}
    - {dirname: "jekyll-sites"}
  notify:
    - restart-nfs-server

- name: "{{ project_name }} | The NFS server exports definition file is present"
  ansible.builtin.template:
    src:  "nfs-project.exports.j2"
    dest: "/etc/exports.d/{{ project_name }}.vagrant-k8s-lab.exports"
  notify:
    - restart-nfs-server

- name: "{{ project_name }} | The jekyll project is owned by vagrant user"
  # NOTE: During the jekyll build process, the src/.jekyll-cache directory is
  # created. If permissions in jekyll project aren't set correctly the
  # directory creation fails, and so do the build.
  ansible.builtin.file:
    path: "/srv/exports/{{ item.dirname }}/{{ project_name }}"
    state: directory
    owner: vagrant
    group: vagrant
    recurse: true
  notify:
    - restart-nfs-server
  loop:
    - {dirname: "jekyll-repos"}
    - {dirname: "jekyll-sites"}

- name: "{{ project_name }} | The jekyll project name resolution is present"
  ansible.builtin.template:
    src: "dnsmasq-project.vagrant-k8s-lab.conf.j2"
    dest: "/etc/dnsmasq.d/{{ project_name }}.vagrant-k8s-lab.conf"
  notify:
    - restart-dnsmasq-server

- name: "{{ project_name }} | The jekyll project domain name is present"
  ansible.builtin.template:
    src: "dnsmasq-hosts-project.vagrant-k8s-lab.j2"
    dest: "/etc/hosts-{{ project_name }}.vagrant-k8s-lab"
  notify:
    - restart-dnsmasq-server

# NFS exports must be ready for jekyll pods to use. Otherwise access
# denied or timeout errors may result in pod creation failures. Let's flush
# handlers before deploying jekyll.
- name: "Flush handlers"
  meta: flush_handlers

# ------------------------------------------------------------------------------
# Kubernetes resources
# ------------------------------------------------------------------------------
- ansible.builtin.include_tasks:
    file: manifest-jekyll.yml
    apply:
      become: false
