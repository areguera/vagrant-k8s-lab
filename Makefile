SCENARIO := default

all: scenario.status

.PHONY: ansible
ansible:
	install -d ansible/collections
	ansible-galaxy collection install -r ansible/requirements.yml -p ansible/collections
	ansible-galaxy role install -r ansible/requirements.yml -p ansible/roles

scenario_controlplane.status: ansible scenario/$(SCENARIO)/controlplane.yml
	$(MAKE) -C scenario/$(SCENARIO)/ NODE=controlplane
	@echo "$(shell date) - The areguera/k8s-lab-controlplane vagrant box image was successfully built!" > $@

scenario_workernode.status: ansible scenario/$(SCENARIO)/workernode.yml
	$(MAKE) -C scenario/$(SCENARIO)/ NODE=workernode
	@echo "$(shell date) - The areguera/k8s-lab-workernode vagrant box image was successfully built!" > $@

scenario.status: ansible scenario_controlplane.status scenario_workernode.status Vagrantfile
	vagrant up --provision
	@echo "$(shell date) - The kubernetes cluster was successfully deployed!" > $@

clean:
	$(RM) *.status
	vagrant destroy -f
	vagrant box remove -f areguera/k8s-lab-workernode && $(RM) scenario_workernode.status || true
	vagrant box remove -f areguera/k8s-lab-controlplane && $(RM) scenario_controlplane.status || true
	$(RM) -rf ansible/collections
	$(RM) -rf ansible/roles/linux-system-roles.*
	$(RM) -rf node_modules
	$(RM) -rf .npm
	$(RM) -rf public

.PHONY: jekyll-server
jekyll-server:
	# -------------------------------------------------------------------------------
	# Public
	# -------------------------------------------------------------------------------
	install -d public
	# -------------------------------------------------------------------------------
	# Run server using jekyll-theme-centos:latest container image
	# -------------------------------------------------------------------------------
	podman run --rm \
		-v $$PWD/public:/public:Z \
		-v $$PWD/docs:/site:Z \
		-p 0.0.0.0:4000:4000 \
		--name jekyll-theme-centos \
		registry.gitlab.com/centos/artwork/centos-web/jekyll-theme-centos:latest \
		bundle exec jekyll serve -H 0.0.0.0 -p 4000 --config /site/_config.yml -s /site -d /public
